package Demo1;

import java.util.Scanner;

public class Number {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int num = scanner.nextInt();
        int count=0;
        for (int i = 0; i < num; i++) {
            if(i%3==0||i%5==0){
                continue;
            }else {
                System.out.print(i+" ");
            }

            count++;
            if(count%5==0){
                System.out.println("\n");
            }

        }
    }
}
