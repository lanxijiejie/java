package Demo2;

public class Number {
    private int num1=10;
    private int num2=5;

    public Number() {

    }

    public int  addition(int num1,int num2){
        return (num1+num2);
    }
    public int subtration(int num1,int num2){
        return (num1-num2);
    }
    public  int multiplication(int num1,int num2){
        return (num1*num2);
    }

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public Number(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public int division(int num1, int num2){
        return (num1/num2);
    }
}
