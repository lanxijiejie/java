package Demo2;

public class ParitySum {
    public static void main(String[] args) {
        int sum1=0;
        int sum2=0;
        for (int i = 1; i <= 100; i++) {
            if(i%2==0){
                sum1+=i;
            }else {
                sum2+=i;
            }
        }
        System.out.println("偶数和为"+sum1+"\t"+"奇数和"+sum2);
    }
}
