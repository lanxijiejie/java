package Demo2;

import java.util.Scanner;

public class Parity {
    public static void main(String[] args) {
        System.out.println("请输入一个整数");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        if(num%2==0){
            System.out.println(num+"是偶数");
        }else {
            System.out.println(num+"是奇数");
        }
    }
}
